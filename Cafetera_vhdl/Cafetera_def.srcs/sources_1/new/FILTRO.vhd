----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2019 18:59:51
-- Design Name: 
-- Module Name: FILTRO - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FILTRO is
     
    Port ( entrada : in STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           salida : out STD_LOGIC_VECTOR (3 downto 0));
end FILTRO;

architecture Behavioral of FILTRO is

component antirrebote
	port(
			bot: in std_logic;
			clk_divid: in std_logic;
			reset: in std_logic;
			antirrebote: out std_logic);
end component;
component flanco
	port(
			boton: in std_logic;
			clk: in std_logic;
			boton_out: out std_logic);
end component;
component clk_divider
	generic(tope: integer range 0 to 2**26-1 := 150);
	port(	clk, reset: in std_logic;
			clk_dividido: out std_logic);
end component;

for all: antirrebote use entity work.antirrebote(behavioral);
for all: flanco use entity work.flanco(behavioral);
for all: clk_divider use entity work.clk_divider(behavioral);

signal p_i: std_logic;
signal aux: std_logic_vector (entrada'range);
begin
	I_clk: clk_divider --1�
		generic map (tope=> 500000)  
		port map(
			clk => clk,
			reset => '0',
			clk_dividido => p_i);
	fil: for i IN entrada'range GENERATE
	I_antirrebote: antirrebote port map(  --2�
			Bot => entrada(i),
			clk_divid=>p_i,
			reset=> '0',
			antirrebote => aux(i));
	I_flanco: flanco port map(   --3�
			boton => aux(i),
			clk => clk,
			boton_out => salida(i));
	end GENERATE;



end Behavioral;

