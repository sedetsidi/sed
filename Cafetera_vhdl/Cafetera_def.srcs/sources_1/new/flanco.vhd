----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2019 18:59:51
-- Design Name: 
-- Module Name: flanco - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flanco is
    Port ( boton : in STD_LOGIC;
           clk : in STD_LOGIC;
           boton_out : out STD_LOGIC);
end flanco;

architecture Behavioral of flanco is

signal boton_1,boton_2: std_logic;  --salidas de biestables
begin

biestable_D1:	process(clk,boton)
		begin
			if (clk'event and clk='1') then
				boton_1<=boton;
			end if;
		end process;

biestable_D2: process(clk,boton_1)
		begin
			if(clk'event and clk='1') then
				boton_2<=boton_1;
			end if;
		end process;
		
boton_out<=boton_1 and (not boton_2);

end Behavioral;
