----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2019 18:59:51
-- Design Name: 
-- Module Name: antirrebote - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity antirrebote is
    Port ( bot : in STD_LOGIC;
           clk_divid : in STD_LOGIC;
           antirrebote : out STD_LOGIC;
           reset: in STD_LOGIC );
end antirrebote;

architecture Behavioral of antirrebote is

signal temp: std_logic :='0';

begin

antirrebote <= temp;

process (clk_divid,bot,reset)
begin
	if reset = '1' then temp <= '0';
	elsif clk_divid = '1' and clk_divid'event then 
		temp <= bot;
	end if;
end process;


end Behavioral;

