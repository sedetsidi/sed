----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2019 17:06:22
-- Design Name: 
-- Module Name: CAFETERA - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CAFETERA is
     generic(	
               t1: integer range 0 to 2**8 - 1 := 15; -- tiempo de apagado cuando no hay actividad seleccion cafe  (en segundos)
               t2: integer range 0 to 2**8 - 1 := 15;-- tiempo de espera para el nivel de azucar
               t3: integer range 0 to 2**8 - 1 := 15;
               t4: integer range 0 to 2**8 - 1 := 10;-- tiempo cafe corto
               t5: integer range 0 to 2**8 - 1 := 20;-- tiempo cafe largo
               t6: integer range 0 to 2**8 - 1 := 15;-- tiempo si no se selecciona complemento
               t7: integer range 0 to 2**8 - 1 := 5;--tiempo de complemento1
               t8: integer range 0 to 2**8 - 1 := 7);--tiempo seleccion edulcorante
               
    Port ( reset : in STD_LOGIC := '0';
           boton : in STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           display1 : out STD_LOGIC_VECTOR (7 downto 0);
           display2 : out STD_LOGIC_VECTOR (7 downto 0);
           display3 : out STD_LOGIC_VECTOR (7 downto 0);
           display4 : out STD_LOGIC_VECTOR (7 downto 0);
           led: out STD_LOGIC_VECTOR (7 downto 0));
end CAFETERA;



architecture Behavioral of CAFETERA is

type estado is (E0, E1, E2, E3, E4, E5, E6);
signal estado_actual, estado_siguiente: estado:=E0;
constant second: integer:= 50000000; 
signal contador: integer range 0 to 2**26 - 1 := 0;
signal tiempo: integer range 0 to 60 := 0; -- en segundos
signal tiempo_restante: integer range 0 to 60 :=0;


signal nivel_dulce: integer range 0 to 3 :=2;
type edulcorante is (azucar,sacarina);
signal tipo_edulc_reg: edulcorante:=azucar;
type cafe is (corto, largo);
signal tipo_cafe: cafe:=corto;
type complemento1 is (leche_en, leche_sem, leche_des, coco, sin);
signal tipo_complemento1: complemento1:=sin;

begin
clock: process(clk) --ACTUALIZACION ESTADO
			begin
			if (clk'event and clk='1') then
				estado_actual <= estado_siguiente;
			end if;
			end process;
			

Estados: process(clk)
		begin
			if clk='1' and clk'event then
			if reset='1' then
			   estado_siguiente<=E0;
			   contador<=0;
			   tiempo<=0;
			else
			 case estado_actual is
				when E0 => --REPOSO
						 --empieza a contar el tiempo, va sumando al contador en us y cuando es mayor que 50000000 suma uno al tiempo en segundos
							contador<= contador+1;
							if contador >= second then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- si se aprieta cualquier boton se enciende la m�quina
						
						if boton/="0000" then estado_siguiente<=E1;
							contador<=0; tiempo<=0;
						end if;
				when E1 => --SELECCION TIPO DE CAFE
							contador<= contador+1;
							if contador >= second then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- pulsar para el tipo de cafe
						if boton="0001" then tipo_cafe<= corto;
								estado_siguiente<=E2; contador<=0; tiempo<=0;
						elsif boton="1000" then tipo_cafe<= largo;
								estado_siguiente<=E2; contador<=0; tiempo<=0;
						elsif tiempo >= t1 then
								estado_siguiente<=E0; contador<=0; tiempo<=0;
						end if;


			
						
				when E2 => --SELECCION EDULCORANTE
							contador<= contador+1;
							if contador >= second then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- pulsar para el tipo de edulcorante
						if boton="0001" then tipo_edulc_reg<= azucar;
								estado_siguiente<=E3; contador<=0; tiempo<=0;
						elsif boton="1000" then tipo_edulc_reg<= sacarina;
								estado_siguiente<=E3; contador<=0; tiempo<=0;
						elsif tiempo >= t8 then
								estado_siguiente<=E3; contador<=0; tiempo<=0;
						end if;
				when E3 => --SELECCION NIVEL DE EDULCORANTE
							contador<= contador+1;
							if contador >= second then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- pulsar para el nivel de edulcorante
						if boton="0001" then nivel_dulce<=3; estado_siguiente<=E4; contador<=0; tiempo<=0;
						elsif boton="0010" then nivel_dulce<=2; estado_siguiente<=E4; contador<=0; tiempo<=0;
						elsif boton="0100" then nivel_dulce<=1;estado_siguiente<=E4; contador<=0; tiempo<=0;
						elsif boton="1000" then nivel_dulce<=0;estado_siguiente<=E4; contador<=0; tiempo<=0;
						elsif tiempo >= t2 then
								estado_siguiente<=E4; contador<=0; tiempo<=0; 
						end if;
						
						
				when E4=> --CUENTA ATR�S Y PROCESO DEL CAFE
							contador<= contador+1;
							if contador >= second then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- espera el tiempo de activacion de la bomba
						
						if tipo_cafe=corto then
								tiempo_restante<= t4 - tiempo;
						elsif tipo_cafe=largo then
								tiempo_restante<= t5 -tiempo;
						end if;
						if tipo_cafe=corto and tiempo >= t4 then
								estado_siguiente<=E5; contador<=0; tiempo<=0; 
						elsif tipo_cafe=largo and tiempo >= t5 then
								estado_siguiente<=E5; contador<=0; tiempo<=0; 
						end if;
						
				when E5 => --SELCCION COMPLEMENTO 1    
							contador<= contador+1;
							if contador >= second then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- pulsar para elegir el complemento
						if boton="1000" then tipo_complemento1<= leche_en;
								estado_siguiente<=E6; contador<=0; tiempo<=0;
						elsif boton="0100" then tipo_complemento1<= leche_sem;
								estado_siguiente<=E6; contador<=0; tiempo<=0;
						elsif boton="0010" then tipo_complemento1<= leche_des;
								estado_siguiente<=E6; contador<=0; tiempo<=0;
						elsif boton="0001" then tipo_complemento1<= coco;
								estado_siguiente<=E6; contador<=0; tiempo<=0;
						elsif tiempo >= t6 then tipo_complemento1<=sin;
								estado_siguiente<=E0; contador<=0; tiempo<=0;
						end if;
						
				when E6 => --CUENTA ATRAS TIEMPO Y PROCESO COMPLEMENTO 1
							contador<= contador+1;
							if contador >= second then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- espera el tiempo para servir el complemento
						
						if tipo_complemento1=leche_en then tiempo_restante<= t7 - tiempo;
						elsif tipo_complemento1=leche_sem then tiempo_restante<= t7 - tiempo;
						elsif tipo_complemento1=leche_des then tiempo_restante<= t7 - tiempo;
						elsif tipo_complemento1=coco then tiempo_restante<=t7 - tiempo;
						end if;
						
						if tipo_complemento1=leche_en and tiempo >= t7 then
								estado_siguiente<=E0; contador<=0; tiempo<=0;
						elsif tipo_complemento1=leche_sem and tiempo >= t7 then
								estado_siguiente<=E0; contador<=0; tiempo<=0;
						elsif tipo_complemento1=leche_des and tiempo >= t7 then
								estado_siguiente<=E0; contador<=0; tiempo<=0;
						elsif tipo_complemento1=coco and tiempo >= t7 then
								estado_siguiente<=E0; contador<=0; tiempo<=0;
						end if;
						end case;
					end if;
			end if;			
			
		end process;
	displays: process(clk)
            begin
                if clk'event and clk='1' then
                case estado_actual is
                    when E0 => led<="00000001";
                                     display1<= "11000110"; --C
                                     display2<= "10001000"; --A
                                     display3<= "10001110"; --F
                                     display4<= "10000110"; --E
                    when E1 => 
                            led<="00000010";
                            display1<= "11000111"; --L
                            display2<= "11111111"; --apagado
                            display3<= "11111111"; --apagado
                            display4<= "11000110"; --C
                    
                                    
                    when E2 => 
                                
                                    led<="00000100";
                                    display1<= "10010010"; --S
                                    display2<= "11111111"; --apagado
                                    display3<= "11111111"; --apagado
                                    display4<= "10001000"; --A
                    when E3 =>
                                    led<="00001000";
                                    display1<= "11111001"; --1
                                    display2<= "10100100"; --2
                                    display3<= "10110000"; --3
                                    display4<= "10011001"; --4
                                    
                    when E4 => --TIPO CAFE-CUENTA ATRAS
                                    led<="10010000";
                                    case tipo_cafe is
                                        when corto => display1<="11000110"; 
                                        when others => display1<="11000111";
                                    end case;
                                    
                                    display2<="10111111";
                                    
                                    
                                    case tiempo_restante is
                                        when 0 to 9 =>display3<="11000000";
                                        when 10 to 19 =>display3<="11111001";
                                        when 20=> display3<="10100100";
                                        when others => display3<= "01111111";
                                    end case;
                                    
                                    case tiempo_restante is
                                        when 0 | 10 | 20 =>display4<= "11000000";
                                        when 1 | 11 =>display4<= "11111001";
                                        when 2 | 12 =>display4<= "10100100";
                                        when 3 | 13 =>display4<= "10110000";
                                        when 4 | 14 =>display4<= "10011001";
                                        when 5 | 15 =>display4<= "10010010";
                                        when 6 | 16 =>display4<= "10000010";
                                        when 7 | 17 =>display4<= "11111000";
                                        when 8 | 18 =>display4<= "10000000";
                                        when 9 | 19 =>display4<= "10011000";
                                        when others =>display4<= "01111111";
                                    end case;
        
        
                    when E5 => --SELECCION COMPLEMENTO 1
                                    led<="00100000";
                                    case tiempo is
                                        when  0 | 1 | 2 | 3  => 
                                                    display1<= "10000110"; --E
                                                    display2<= "10101011"; --n
                                                    display3<= "10000111"; --t
                                                    display4<= "10000110"; --E
                                        when 4 | 5 | 6 | 7 => 
                                                    display1<= "10010010"; --S
                                                    display2<= "10000110"; --E
                                                    display3<= "10101011"; --n
                                                    display4<= "11001111"; --i
                                        when 8 | 9 | 10 | 11 => 
                                                    display1<= "10100001"; --d
                                                    display2<= "10000110"; --E
                                                    display3<= "10010010"; --S
                                                    display4<= "10101011"; --n
                                        when 12 | 13 | 14 | 15 => 
                                                    display1<= "11000110"; --C 
                                                    display2<= "11000000"; --O
                                                    display3<= "11000110"; --C
                                                    display4<= "11000000"; --O
                                        when others => 
                                                    display1<= "01111111"; --apagado
                                                    display2<= "01111111"; --apagado
                                                    display3<= "01111111"; --apagado
                                                    display4<= "01111111"; --apagado
                                    end case;
                                    
                    when E6 => --TIPO COMPLEMENTO 1 - CUENTA ATRAS
                                    led<="10100000";
                                    display2<="10111111";
                                    case tipo_complemento1 is
                                        when leche_en =>
                                             display1<= "10000110";    
                                        when leche_sem =>
                                             display1<= "10010010";    
                                        when leche_des =>
                                             display1<= "10100001";    
                                        when coco =>
                                             display1<= "11000110";
                                        when others =>
                                             display1<="01111111";
                                    end case;
                                    display3<="10111111";
        
                                    case tiempo_restante is
                                        when 0     =>display4<= "11000000";
                                        when 1  =>display4<= "11111001";
                                        when 2  =>display4<= "10100100";
                                        when 3  =>display4<= "10110000";
                                        when 4  =>display4<= "10011001";
                                        when 5     =>display4<= "10010010";
                                        when others =>display4<= "01111111";
                                    end case;
                    
                    
                end case;
                end if;
            end process;
        
end Behavioral;



