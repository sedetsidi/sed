----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2019 17:18:23
-- Design Name: 
-- Module Name: multip4a1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity multip4a1 is
    Port ( clk: in std_logic;
           disp1 : in STD_LOGIC_VECTOR (7 downto 0);
           disp2 : in STD_LOGIC_VECTOR (7 downto 0);
           disp3 : in STD_LOGIC_VECTOR (7 downto 0);
           disp4 : in STD_LOGIC_VECTOR (7 downto 0);
           sel : in STD_LOGIC_VECTOR (1 downto 0);
           seg : out STD_LOGIC_VECTOR (7 downto 0));
end multip4a1;

architecture Behavioral of multip4a1 is

begin
process(disp1,disp2,disp3,disp4,sel)
		begin
			case sel IS
				when "00" => seg <= disp1;
				when "01" => seg <= disp2;
				when "10" => seg <= disp3;
				when others => seg <= disp4;
			end case;
		end process;


end Behavioral;
