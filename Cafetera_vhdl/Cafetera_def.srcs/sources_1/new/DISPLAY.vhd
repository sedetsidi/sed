----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2019 17:18:23
-- Design Name: 
-- Module Name: DISPLAY - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DISPLAY is
    Port ( clk : in STD_LOGIC;
           display1 : in STD_LOGIC_VECTOR (7 downto 0);
           display2 : in STD_LOGIC_VECTOR (7 downto 0);
           display3 : in STD_LOGIC_VECTOR (7 downto 0);
           display4 : in STD_LOGIC_VECTOR (7 downto 0);
           pantalla : out STD_LOGIC_VECTOR (7 downto 0);
           control : out STD_LOGIC_VECTOR (3 downto 0));
end DISPLAY;

architecture Behavioral of DISPLAY is

component multip4a1
	port(	disp1: in std_logic_vector (7 downto 0);
			disp2: in std_logic_vector (7 downto 0);
			disp3: in std_logic_vector (7 downto 0);
			disp4: in std_logic_vector (7 downto 0);
			sel:	in	std_logic_vector(1 downto 0);
			seg: out std_logic_vector (7 downto 0));
	end component;
	
COMPONENT decod2a4
	PORT(
		input : IN std_logic_vector(1 downto 0);
		enable: IN std_logic;          
		output : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
	COMPONENT state_change
	PORT(
		clk_divid : IN std_logic;          
		state : OUT std_logic_vector(1 downto 0)
		);
	END COMPONENT;
	
COMPONENT clk_divider
	generic (tope: integer range 0 to 2**26-1 := 10);
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;          
		clk_dividido : OUT std_logic
		);
	END COMPONENT;
		
for all: multip4a1 use entity work.multip4a1(Behavioral);
for all: decod2a4 use entity work.decod2a4(Behavioral);
for all: clk_divider use entity work.clk_divider(Behavioral);
for all: state_change use entity work.state_change(Behavioral);
	


signal aux1: std_logic_vector(1 downto 0);
signal aux2: std_logic;
begin

I_multip4a1: multip4a1 PORT MAP(--3�
		disp1 => display1,
		disp2 => display2,
		disp3 => display3,
		disp4 => display4,
		sel => aux1,
		seg => pantalla
	); 

I_decod2a4: decod2a4 PORT MAP(--3�
		input => aux1,
		enable => '0',
		output => control
	);
	
I_state_change: state_change PORT MAP( --2�
		clk_divid => aux2,
		state => aux1 --(sale 00,01,10,11)
	);
	
I_divisor_reloj: clk_divider --1�
	generic map( tope => 50000)
	PORT MAP(
		clk => clk,
		reset => '0',
		clk_dividido => aux2
	);



end Behavioral;

