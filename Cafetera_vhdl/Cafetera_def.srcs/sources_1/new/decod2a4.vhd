----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2019 17:18:23
-- Design Name: 
-- Module Name: decod2a4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decod2a4 is
    generic(var: bit := '0');
    Port ( input : in STD_LOGIC_VECTOR (1 downto 0);
           enable : in STD_LOGIC;
           output : out STD_LOGIC_VECTOR (3 downto 0));
end decod2a4;

architecture Behavioral of decod2a4 is

begin
process(input,enable)
	variable noen: std_logic;
	variable output_i: std_logic_vector (output'range);
	begin
		noen := not (enable); 
		if noen = '0' then output_i:= "0000";   
			elsif input="00" then output_i:="0001";  
			elsif input="01" then output_i:="0010"; 
			elsif input="10" then output_i:="0100"; 
			else output_i:="1000";
		end if;
		if var='0' then output<=not (output_i); --salida negada displays
			else output<= output_i;
		end if;
	end process;

end Behavioral;
