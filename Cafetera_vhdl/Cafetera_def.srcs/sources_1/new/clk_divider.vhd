----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.01.2019 18:59:51
-- Design Name: 
-- Module Name: clk_divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_divider is
    generic (tope: integer range 0 to 2**26-1 := 3);
    
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           clk_dividido : out STD_LOGIC);
end clk_divider;

architecture Behavioral of clk_divider is

signal contador: natural range 0 to 2**26-1 := 0;
			
begin

divisor:	process(clk,reset)
			begin
				if reset = '1' then
					contador<=0;
					clk_dividido<='0';
				elsif clk'event and clk='1' then
					if contador >= tope - 1 then
						clk_dividido<='1';
						contador<=0;
					else
						clk_dividido<='0';
						contador<=contador + 1;
					end if;
				end if;
			end process;


end Behavioral;
