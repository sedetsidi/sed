----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2019 17:24:54
-- Design Name: 
-- Module Name: state_change - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity state_change is
    Port ( clk_divid : in STD_LOGIC;
           state : out STD_LOGIC_VECTOR (1 downto 0));
end state_change;

architecture Behavioral of state_change is

signal contador: natural range 0 to 3 :=0;
begin 
	process(clk_divid)
	begin
		if clk_divid = '1' and clk_divid'event then
			if contador = 0 then state<="00";
				contador<=contador +1 ;
			elsif contador = 1 then 
				state<="01";
				contador<=contador +1;
			elsif contador = 2 then
				state<="10";
				contador<=contador +1;
			else
				state<="11";
				contador<=0;
			end if;
		end if;
	end process;


end Behavioral;

