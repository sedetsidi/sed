library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------
entity tb_divisor_reloj is
end tb_divisor_reloj;
------------------------------------------------------------------
--arquitectura
architecture behavioral of tb_divisor_reloj is

--declaracion de componentes a examinar
component clk_divider is
	port(	clk,reset: in std_logic;
			clk_dividido: out std_logic);
end component;

--se�ales que vamos a generar: inputs:
signal clk: std_logic :='0';
signal reset: std_logic :='0';
--se�ales que vamos a comprobar si estan bien: outputs:
signal clk_dividido: std_logic;
 -- Clock period definitions

begin
--instanciacion:
uut: clk_divider port map(
	clk=> clk,
	reset=> reset,
	clk_dividido=> clk_dividido);


P_RESET: process
			begin
				reset<='0';
				wait for 33 ns;
				reset<='1';
				wait for 89 ns;
				reset<='0';
				assert false report "puesto el reset" severity note;
				wait;-- espera para siempre
			end process;

P_Clk: Process
			begin
				clk <= '1';
				wait for 10 ns;
				clk <= '0';
				wait for 10 ns;
			end process;
			


end behavioral;