LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_Maquina_cafe IS
END tb_Maquina_cafe;
 
ARCHITECTURE behavior OF tb_Maquina_cafe IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Maq_cafe
    PORT(
         clk : IN  std_logic;
         Pulsador : IN  std_logic_vector(3 downto 0);
         salida_fin: OUT  std_logic_vector(7 downto 0);
         control_fin : OUT  std_logic_vector(3 downto 0);
         led_fin : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal Pulsador : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal salida_fin : std_logic_vector(7 downto 0);
   signal control_fin : std_logic_vector(3 downto 0);
   signal led_fin : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Maq_cafe PORT MAP (
          clk => clk,
          Pulsador => Pulsador,
          salida_fin => salida_fin,
          control_fin => control_fin,
          led_fin => led_fin
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      --wait for 100 ns;	
			
      wait for clk_period*10;
		
      -- insert stimulus here 
					Pulsador<="0100";
					wait for 40 ns;
					Pulsador<="1000";
					wait for 40ns;
                    Pulsador<="0001";
                    wait for 40ns;
                    Pulsador<="0100";
	
      wait;
   end process;

END;
