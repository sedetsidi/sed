LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY display_tb IS
END display_tb;
 
ARCHITECTURE behavioral OF display_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT display
    PORT(
         display1 : IN  std_logic_vector(7 downto 0);
         display2 : IN  std_logic_vector(7 downto 0);
         display3 : IN  std_logic_vector(7 downto 0);
         display4 : IN  std_logic_vector(7 downto 0);
         clk : IN  std_logic;
         pantalla : OUT  std_logic_vector(7 downto 0);
         control : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal display1 : std_logic_vector(7 downto 0) := (others => '0');
   signal display2 : std_logic_vector(7 downto 0) := (others => '0');
   signal display3 : std_logic_vector(7 downto 0) := (others => '0');
   signal display4 : std_logic_vector(7 downto 0) := (others => '0');
   signal clk : std_logic := '0';

 	--Outputs
   signal pantalla : std_logic_vector(7 downto 0);
   signal control : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: display PORT MAP (
          display1 => display1,
          display2 => display2,
          display3 => display3,
          display4 => display4,
          clk => clk,
          pantalla => pantalla,
          control => control
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		

      -- insert stimulus here 
		wait for 40 ns;
		display1<="00110001";
		display2<="00110000";
		display3<="00110011";
		display4<="00110101";

      wait;
   end process;

END;
