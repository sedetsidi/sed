----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.01.2019 13:36:03
-- Design Name: 
-- Module Name: CAFETERA_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


ENTITY cafetera_tb IS
END cafetera_tb;
 
ARCHITECTURE behavioral OF cafetera_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT cafetera
    PORT(
         clk : IN  std_logic;
         boton : IN  std_logic_vector(3 downto 0);
         display1 : OUT  std_logic_vector(7 downto 0);
         display2 : OUT  std_logic_vector(7 downto 0);
         display3 : OUT  std_logic_vector(7 downto 0);
         display4 : OUT  std_logic_vector(7 downto 0);
         led : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal boton : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal display1 : std_logic_vector(7 downto 0);
   signal display2 : std_logic_vector(7 downto 0);
   signal display3 : std_logic_vector(7 downto 0);
   signal display4 : std_logic_vector(7 downto 0);
   signal led : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cafetera PORT MAP (
          clk => clk,
          boton => boton,
          display1 => display1,
          display2 => display2,
          display3 => display3,
          display4 => display4,
          led => led
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 
		boton<="0010";
		wait for 40 ns;
		boton<="1000";
		wait for 40ns;
		boton<="0001";
		wait for 20ns;
		boton<="0100";
--		wait for 40ns;
--		boton<="0001";
--		wait for 80ns;
--		boton<="1000";
		
		

      wait;
   end process;

END;