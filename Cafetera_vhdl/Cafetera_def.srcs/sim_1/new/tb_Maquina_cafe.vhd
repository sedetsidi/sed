----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.01.2019 12:41:30
-- Design Name: 
-- Module Name: tb_Maquina_cafe - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_Maquina_cafe is
    Port ( pulsador : in STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           salida_fin : out STD_LOGIC_VECTOR (7 downto 0);
           control_fin : out STD_LOGIC_VECTOR (3 downto 0);
           led_fin : out STD_LOGIC_VECTOR (7 downto 0));
end tb_Maquina_cafe;

architecture Behavioral of tb_Maquina_cafe is

begin


end Behavioral;
