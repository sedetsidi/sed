----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.01.2019 13:52:29
-- Design Name: 
-- Module Name: DISPLAY_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DISPLAY_tb is
    Port ( clk : in STD_LOGIC;
           display1 : in STD_LOGIC_VECTOR (7 downto 0);
           display2 : in STD_LOGIC_VECTOR (7 downto 0);
           display3 : in STD_LOGIC_VECTOR (7 downto 0);
           display4 : in STD_LOGIC_VECTOR (7 downto 0);
           pantalla : out STD_LOGIC_VECTOR (7 downto 0);
           control : out STD_LOGIC_VECTOR (3 downto 0));
end DISPLAY_tb;

architecture Behavioral of DISPLAY_tb is

begin


end Behavioral;
